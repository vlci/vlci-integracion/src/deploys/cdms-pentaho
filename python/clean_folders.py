import os
import shutil

from utils import cargar_propiedades, init_parser


def limpiar_carpetas_tmp(tmp_folder):
    if os.path.exists(tmp_folder):
        shutil.rmtree(tmp_folder)
        os.makedirs(tmp_folder)
    print('>>> Paso previo: Ficheros temporales eliminados correctamente')


if __name__ == '__main__':
    parser = init_parser()
    args = parser.parse_args()
    properties = cargar_propiedades()
    limpiar_carpetas_tmp(properties['common']['tempDir'][args.env])
