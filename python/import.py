import subprocess

from pentaho_api import (configure, get_acl, login_pentaho, modify_acl,
                         move_folder_to_trash)
from utils import cargar_propiedades, init_parser


def import_to_pentaho(server_path, url, ruta_zip, remote_dir, dashboard, user, password):
    print('>>> Inicializando importación a Pentaho')
    try:  # Import file

        args = server_path \
            + 'import-export.sh --import --url='+url \
            + ' --username=' + user \
            + ' --password=' + password \
            + ' --file-path=' + ruta_zip \
            + '.zip --charset=UTF-8 --path=/public/' \
            + remote_dir \
            + '/' + dashboard \
            + ' --permission=true --overwrite=true --retainOwnership=true'

        output = subprocess.run(args,
                                check=True,
                                capture_output=True,
                                text=True,
                                shell=True).stdout
        if 'Import was successful' not in output:
            # El comando no provoca error pero no se importa nada
            print(
                '>>> ERROR en la importación. El comando no provoca error pero no se importa nada')
            print(output)
        else:  # Se importa la carpeta
            print(output)

    except Exception as e:  # El comando NO funciona
        print(output)
        print(e)


if __name__ == '__main__':
    parser = init_parser()
    args = parser.parse_args()
    properties = cargar_propiedades()

    pentaho_endpoints = properties['common']['pentahoEndpoints']
    pentaho_files_fields = properties['common']['pentahoFilesFields']
    server_path = properties['common']['serverPath'][args.env]
    temp_dir = properties['common']['tempDir'][args.env]

    url = properties[args.dest]['url'][args.env]
    dashboard = properties[args.dest]['projects'][args.project]['name']
    remote_dir = properties[args.dest]['remoteDir'][args.env]

    ruta_zip = temp_dir + dashboard

    config = configure(url,
                       dashboard,
                       remote_dir,
                       pentaho_endpoints,
                       pentaho_files_fields,
                       {'user': args.user, 'pass': args.password})

    print(config)

    login_response = login_pentaho(config)

    if login_response.status_code == 200:
        acl_info = get_acl(config)

        if acl_info:
            move_folder_to_trash(config)

            import_to_pentaho(server_path,
                              url,
                              ruta_zip,
                              remote_dir,
                              dashboard,
                              args.user,
                              args.password)

            modify_acl(config, acl_info)
