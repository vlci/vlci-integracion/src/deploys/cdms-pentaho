import os

import git  # importar GitPython
from utils import cargar_propiedades, init_parser


def clonar_repositorio(new_folder, git_branch, base_url, project):

    repo_name = base_url + project + ".git"

    print(">>> Clonando rama " + git_branch)
    os.makedirs(new_folder)
    repo = git.Repo.clone_from(
        repo_name, new_folder, branch=git_branch)
    print("    Rama " + git_branch + " clonada")
    return repo


def tag_repositorio(repo, tagname, comments):
    new_tag = repo.create_tag(tagname, message=comments)
    repo.remotes.origin.push(new_tag)
    print("    Tag " + tagname + " creado")


def merge_test_main(repo):
    repo.git.checkout('main')
    repo.git.merge('origin/test')
    repo.remotes.origin.push()
    print("    Merge de la rama TEST en la rama MAIN hecho")


if __name__ == '__main__':
    parser = init_parser()
    args = parser.parse_args()
    properties = cargar_propiedades()

    new_folder = os.path.join(
        properties['common']['tempDir'][args.env],
        properties['common']['repoTempDirName'][args.env])

    git_branch = properties['common']['repoBranch'][args.env]

    base_url = properties[args.dest]['repository']['baseurl']
    prj_folder = properties[args.dest]['repository']['project'][args.project]

    repo = clonar_repositorio(new_folder, git_branch,
                              base_url, prj_folder)
    if args.env == "PRO":
        merge_test_main(repo)
        tag_repositorio(repo, args.tagname, args.comments)
