import json
from argparse import ArgumentParser


def cargar_propiedades():
    with open('config/deploy_properties.json') as file:
        properties = json.load(file)
    print('>>> Carga de properties')
    return properties


def cargar_secretos():
    with open('config/secrets_properties.json') as file:
        secrets = json.load(file)
    print('>>> Carga de secretos')
    return secrets


def init_parser():
    parser = ArgumentParser()
    parser.add_argument('-d', '--dest', help='Pentaho Server to deploy',
                        choices=['cdm', 'cda'], required=True)
    parser.add_argument('-e', '--env', help='Enviroment to deploy',
                        choices=['PRE', 'PRO', 'LOCAL'], required=True)
    parser.add_argument('-j', '--project', help='Project to deploy',
                        choices=['alcaldia', 'alcaldia_mobilitat', 'ciutat', 'ge',
                                 'trafico', 'unificado', 'portada',
                                 'vminut', 'datosabiertos',
                                 'datos3os'],
                        required=True)
    parser.add_argument('-u', '--user', help='Admin User')
    parser.add_argument('-p', '--password', help='Admin Password')
    parser.add_argument('-t', '--tagname', help='Tag name')
    parser.add_argument('-o', '--comments', help='Git comments for the tag')
    return parser
