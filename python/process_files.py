import codecs
import os
import xml.dom.minidom
import xml.etree.ElementTree

from utils import cargar_propiedades, init_parser


def modificar_ficheros(ruta_repo, data_source,
                       local_dir, remote_dir,
                       cdn_url):
    print('>>> Modificación de ficheros')

    for file in os.listdir(ruta_repo):
        if '.cda' in file:
            # Lee archivo xml
            dom = xml.dom.minidom.parse(os.path.join(ruta_repo, file))
            root = dom.documentElement
            # Asigna el nuevo valor a la etiqueta Jndi
            lista_jndi = root.getElementsByTagName('Jndi')
            for jndi in lista_jndi:
                if jndi.firstChild.data == 'valLocal':
                    jndi.firstChild.data = data_source

                # TO-DO Es necesario este replace?
                if jndi.firstChild.data == 'valUserLocal':
                    jndi.firstChild.data = 'valUserPro'

            # Modifica y guarda el archivo
            xml_specific = os.path.join(ruta_repo, file)
            with codecs.open(xml_specific, 'w', "UTF-8") as fh:
                dom.writexml(fh, encoding="UTF-8")

        elif '.cdfde' in file:
            name = os.path.join(ruta_repo, file)
            with open(name, 'r', encoding='utf-8') as old_file:
                x = old_file.read()
                with open(name, 'w', encoding='utf-8') as new_file:
                    new_line = x.replace(local_dir, remote_dir)

                    new_line = new_line.replace(
                        "\"value\" : \"valLocal\"",
                        "\"value\" : \"" + data_source + "\"")

                    # TO-DO Es necesario este replace?
                    new_line = new_line.replace(
                        "\"value\" : \"valUserLocal\"",
                        "\"value\" : \"valUserPro\"")

                    new_line = new_line.replace(
                        'http://localhost:8888',
                        cdn_url)

                    new_file.write(new_line)
                    new_file.close()
                old_file.close()


if __name__ == '__main__':
    parser = init_parser()
    args = parser.parse_args()
    properties = cargar_propiedades()

    repo_tmp_dir = os.path.join(
        properties['common']['tempDir'][args.env],
        properties['common']['repoTempDirName'][args.env])

    data_source = properties['common']['dataSource'][args.env]

    local_dir = properties[args.dest]['remoteDir']['LOCAL']
    remote_dir = properties[args.dest]['remoteDir'][args.env]

    cdn_url = properties['common']['javascriptCDN'] + \
        args.project + \
        properties['common']['javascriptCDNTag'][args.env]

    modificar_ficheros(repo_tmp_dir, data_source,
                       local_dir, remote_dir,
                       cdn_url)
