import codecs
import os
import re
import shutil
import subprocess
import xml.dom.minidom
import xml.etree.ElementTree
from zipfile import ZipFile

from utils import cargar_propiedades, cargar_secretos, init_parser


def modificar_ficheros(ruta_repo_cdm, url, cdn_filepath_legacy):
    print('>>> Modificación de ficheros')
    cdn_url_LOCAL = 'http://localhost:8888'
    for file in os.listdir(ruta_repo_cdm):
        if '.cda' in file:
            # Lee archivo xml
            dom = xml.dom.minidom.parse(os.path.join(ruta_repo_cdm, file))
            root = dom.documentElement
            # Asigna el nuevo valor a la etiqueta Jndi
            lista_jndi = root.getElementsByTagName('Jndi')
            for jndi in lista_jndi:
                if jndi.firstChild.data == 'valPre' or jndi.firstChild.data == 'valPro':
                    jndi.firstChild.data = 'valLocal'

                if jndi.firstChild.data == 'valUserPro':
                    jndi.firstChild.data = 'valUserLocal'
            # Con los nuevos valores en las etiquetas, aplico los cambios
            xml_specific = os.path.join(ruta_repo_cdm, file)
            with codecs.open(xml_specific, 'w', "UTF-8") as fh:
                dom.writexml(fh, encoding="UTF-8")

        elif '.cdfde' in file:
            with open(os.path.join(ruta_repo_cdm, file), 'r', encoding='utf-8') as old_file:
                x = old_file.read()
                with open(os.path.join(ruta_repo_cdm, file), 'w', encoding='utf-8') as new_file:
                    new_line = x.replace(
                        "\"value\" : \"valPro\"", "\"value\" : \"valLocal\"")

                    cdn_url = url
                    new_line = new_line.replace(
                        "\"value\" : \"valUserPro\"", "\"value\" : \"valUserLocal\"").replace(
                            cdn_url+cdn_filepath_legacy, cdn_url_LOCAL+cdn_filepath_legacy)
                    new_file.write(new_line)
                    new_file.close()
                old_file.close()


def comprimir(cdm_name, ruta_repo_cdm):
    # Crea el zip dentro de la carpeta del cdm del repo tmp de la rama correspondiente
    print('>>> Comprimiendo ficheros')
    directory_name = os.path.join(
        ruta_repo_cdm, cdm_name)
    os.makedirs(directory_name)
    # Se copian los archivos menos el .git en una carpeta auxiliar en la carpeta temporal
    for i in os.listdir(ruta_repo_cdm):
        if '.git' not in i and i != cdm_name:
            if os.path.isdir(os.path.join(ruta_repo_cdm, i)):
                shutil.copytree(os.path.join(ruta_repo_cdm, i),
                                os.path.join(directory_name, i))
            else:
                shutil.copy(os.path.join(ruta_repo_cdm, i),
                            os.path.join(directory_name, i))
    # Se crea el zip y se deja en la misma carpeta temporal
    shutil.make_archive(directory_name, 'zip', directory_name)
    shutil.rmtree(directory_name)  # Se elimina la carpeta, dejando solo el zip
    return directory_name + '.zip'


def descomprimir(ruta_zip, ruta_repo_cdm, cdm):
    print('>>> Descomprimiendo ficheros')
    # Se mueven al repositorio solo los archivos de la carpeta del cdm
    if cdm == 'trafico':
        ruta_carpeta = os.path.join(ruta_repo_cdm, 'mobilitat')
        pattern = '^' + 'mobilitat' + '/.'
    else:
        ruta_carpeta = os.path.join(ruta_repo_cdm, cdm)
        pattern = '^' + cdm + '/.'
    zip = ZipFile(ruta_zip)
    for file in zip.namelist():
        if re.match(pattern, file, re.IGNORECASE):  # Se extrae solo la carpeta del cdm
            zip.extract(file, ruta_repo_cdm)
    zip.close()

    # Se copia el contenido de la carpeta en el repositorio
    for i in os.listdir(ruta_carpeta):
        if os.path.isdir(os.path.join(ruta_carpeta, i)):
            shutil.copytree(os.path.join(ruta_carpeta, i), os.path.join(
                ruta_repo_cdm, i), dirs_exist_ok=True)
        else:
            shutil.copy(os.path.join(ruta_carpeta, i),
                        os.path.join(ruta_repo_cdm, i))

    # Se elimina la carpeta extraida y el zip, dejando solo el conteniudo en el repositorio
    shutil.rmtree(ruta_carpeta)
    os.remove(ruta_zip)


def import_to_pentaho(server_path, url, remote_dir, cdm_name, ruta_zip, type, secrets):
    print('>>> Inicializando importación a Pentaho Local')
    user = secrets[type]['user']['LOCAL']
    passwd = secrets[type]['pass']['LOCAL']
    try:  # Import file
        args = server_path + 'import-export.bat --import --url=' + url + \
            ' --username=' + user + ' --password=' + passwd + \
            ' --file-path=' + ruta_zip + ' --charset=UTF-8 --path=/public/' + \
            remote_dir + '/' + cdm_name + \
            ' --permission=true --overwrite=true --retainOwnership=true'
        output = subprocess.run(
            args, check=True, capture_output=True, text=True, shell=True).stdout
        if 'Import was successful' not in output:  # El comando no provoca error pero no se importa nada
            print(
                '>>> ERROR en la importación. El comando no provoca error pero no se importa nada')
            print(output)
        else:  # Se importa la carpeta
            print(output)

    except Exception as e:  # El comando NO funciona
        print(e)


def export_from_pentaho(server_path, url, remote_dir, cdm_name,
                        ruta_repo_cdm, type, secrets):
    print('>>> Inicializando exportación de Pentaho')

    ruta_zip = os.path.join(
        ruta_repo_cdm, cdm_name) + '.zip'

    user = secrets[type]['user']['LOCAL']
    passwd = secrets[type]['pass']['LOCAL']

    try:  # Export file
        args = server_path + 'import-export.bat --export --url=' + url + \
            ' --username=' + user + ' --password=' + passwd + \
            ' --file-path=' + ruta_zip + ' --charset=UTF-8 --path=/public/' + \
            remote_dir + '/' + \
            cdm_name + ' --withManifest=true'
        output = subprocess.run(
            args, check=True, capture_output=True, text=True).stdout
        if 'Export was successful' not in output:  # El comando no provoca error pero no se exporta nada
            print(output)
        else:  # Se exporta la carpeta
            print(output)
    except Exception as e:  # El comando NO funciona
        print(e)
    return ruta_zip


if __name__ == '__main__':
    parser = init_parser()
    properties = cargar_propiedades()
    secrets = cargar_secretos()

    # Carga variables
    task = str(input('Tarea a realizar (IMPORT|EXPORT): ')).lower()
    type = str(input('Cuadro de Mando o CDA (CDM|CDA): ')).lower()
    if type == 'cdm':
        cdm = str(
            input('Cuadro de mando (ALCALDIA|ALCALDIA_MOBILITAT|CIUTAT|GE|TRAFICO|UNIFICADO|PORTADA): ')).lower()

    repo_local = os.environ[properties[type]['projects'][cdm]['repoPathVar']]
    cdm_name = properties[type]['projects'][cdm]['name']

    urlJS = properties['common']['javascriptCDN']+cdm + \
        properties['common']['javascriptCDNTag']['LOCAL']
    cdn_filepath_legacy = properties['common']['cdnFilepathLegacy']

    server_path = properties['common']['serverPath']['LOCAL']
    url_local = properties[type]['url']['LOCAL']
    remote_dir_local = properties[type]['remoteDir']['LOCAL']

    url = properties[type]['url']['LOCAL']
    remote_dir = properties[type]['remoteDir']['LOCAL']

    # Proceso
    if task in ['import', 'export'] and cdm in ['alcaldia', 'alcaldia_mobilitat', 'ciutat', 'ge', 'trafico', 'unificado', 'portada']:
        if task == 'import':  # Importación
            modificar_ficheros(repo_local, urlJS, cdn_filepath_legacy)
            ruta_zip = comprimir(cdm_name, repo_local)
            import_to_pentaho(server_path, url_local, remote_dir_local,
                              cdm_name, ruta_zip, type, secrets)
            # Borra la carpeta del zip que está dentro del repo local
            os.remove(ruta_zip)
        if task == 'export':  # Exportación
            ruta_zip = export_from_pentaho(
                server_path, url, remote_dir, cdm_name,
                repo_local, type, secrets)
            descomprimir(ruta_zip, repo_local, cdm)
            modificar_ficheros(repo_local, urlJS, cdn_filepath_legacy)
