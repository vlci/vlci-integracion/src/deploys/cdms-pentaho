import os
import shutil

from utils import cargar_propiedades, init_parser


# Crea el zip dentro de la carpeta del cdm del repo tmp
# de la rama correspondiente
def comprimir(ruta_repo_cdm, directory_name):
    os.makedirs(directory_name)

    # Se copian los archivos menos el .git en una carpeta auxiliar
    # en la carpeta temporal
    for i in os.listdir(ruta_repo_cdm):
        if '.git' not in i:
            if os.path.isdir(os.path.join(ruta_repo_cdm, i)):
                shutil.copytree(os.path.join(ruta_repo_cdm, i),
                                os.path.join(directory_name, i))
            else:
                shutil.copy(os.path.join(ruta_repo_cdm, i),
                            os.path.join(directory_name, i))

    print('>>> Comprimiendo ficheros')
    # Se crea el zip y se deja en la misma carpeta temporal
    shutil.make_archive(directory_name, 'zip', directory_name)

    # Se elimina la carpeta, dejando solo el zip
    shutil.rmtree(directory_name)

    print('    Compresión de ficheros realizada')
    # print(directory_name)

    return directory_name + '.zip'


if __name__ == '__main__':
    parser = init_parser()
    args = parser.parse_args()
    properties = cargar_propiedades()

    repo_tmp_dir = os.path.join(
        properties['common']['tempDir'][args.env],
        properties['common']['repoTempDirName'][args.env])

    directory_name = os.path.join(
        properties['common']['tempDir'][args.env],
        properties[args.dest]['projects'][args.project]['name'])

    comprimir(repo_tmp_dir, directory_name)
