import warnings
import xml.etree.ElementTree as ET
from datetime import date

import requests
from requests.auth import HTTPBasicAuth

warnings.filterwarnings("ignore")


def configure(url: str,
              dashboard: str,
              remoteDir: str,
              pentahoEndpoints: dict,
              pentahoFilesFields: dict,
              secrets: dict) -> dict:

    url_repo_files = \
        f"{url}{pentahoEndpoints['repoFiles']}"

    configuration = {
        'username': secrets['user'],
        'password': secrets['pass'],
        'auth': HTTPBasicAuth(secrets['user'], secrets['pass']),
        'dashboard': dashboard,
        'login_url': url
        + pentahoEndpoints['login'],
        'repo_files_url': url_repo_files,
        'list_children_url': url_repo_files
        + pentahoEndpoints['public']
        + '/' + remoteDir
        + pentahoEndpoints['listChildren'],
        'delete_url': url_repo_files
        + pentahoEndpoints['delete'],
        'create_url': url_repo_files
        + pentahoEndpoints['publicCreateDir']
        + ':' + remoteDir
        + ':' + dashboard
        + pentahoEndpoints['create'],
        'acl_url': url_repo_files
        + pentahoEndpoints['public']
        + '/' + remoteDir
        + '/' + dashboard
        + pentahoEndpoints['acl'],
        'rename_parameter': pentahoEndpoints['rename'],
        'file_id_field': pentahoFilesFields['idField'],
        'file_path_field': pentahoFilesFields['pathField'],
        'file_name_field': pentahoFilesFields['nameField'],
        'file_node_field': pentahoFilesFields['fileNodeField']
    }
    return configuration


def log_request_response(response: requests.Response, request_action: str):
    print(response.status_code)
    if response.status_code == 200:
        print(f'API {request_action} request correcta')
    else:
        print(f'API {request_action} request ha fallado')
        print('URL request:', response.url)
        print('Status code:', response.status_code)
        print('Error message:', response.text)


def login_pentaho(config: dict) -> requests.Response:
    login_data = {
        'j_username': config['username'],
        'j_password': config['password']
    }
    response = requests.post(config['login_url'],
                             data=login_data,
                             verify=False)
    log_request_response(response, 'login')
    return response


def list_files(url: str, auth: HTTPBasicAuth) -> requests.Response:
    list_files_response = requests.get(url=url, auth=auth, verify=False)
    log_request_response(list_files_response, 'list files')
    return list_files_response


def find_folder_in_xml(xml: ET.Element, config: dict):
    folder_info: dict = {}

    for node in xml.findall(config['file_node_field']):
        if node.find(config['file_name_field']).text == config['dashboard']:
            folder_info = {
                'id': node.find(config['file_id_field']).text,
                'path': node.find(config['file_path_field']).text,
                'name': node.find(config['file_name_field']).text
            }

    return folder_info


def find_folder_info(config: dict) -> dict:

    list_files_response = list_files(config['list_children_url'],
                                     config['auth'])

    if list_files_response.status_code == 200:
        response_xml = ET.fromstring(list_files_response.text)

        return find_folder_in_xml(response_xml, config)

    return {}


def move_folder_to_trash(config: dict) -> None:

    folder_to_delete = find_folder_info(config)

    if folder_to_delete['name'] and folder_to_delete['path']:

        final_rename_url = f"{config['repo_files_url']}{folder_to_delete['path']}{config['rename_parameter']}{folder_to_delete['name']}_{date.today().strftime('%d%m%Y')}"

        rename_folder_response = requests.put(url=final_rename_url,
                                              auth=config['auth'],
                                              verify=False)
        log_request_response(rename_folder_response, 'rename folder')

        delete_folder_response = requests.put(
            url=config['delete_url'],
            data=folder_to_delete['id'],
            auth=config['auth'],
            verify=False)

        log_request_response(delete_folder_response, 'delete folder')

        create_folder_response = requests.put(
            url=config['create_url'],
            auth=config['auth'],
            verify=False)

        log_request_response(create_folder_response, 'create folder')


def modify_acl(config: dict, acl_xml: ET.Element):
    folder_to_modify = find_folder_info(config)

    if folder_to_modify['name'] and folder_to_modify['id']:
        file_id = acl_xml.find(config['file_id_field'])
        file_id.text = folder_to_modify['id']
        headers = {'Content-Type': 'application/xml'}

        acl_request = requests.put(
            url=config['acl_url'],
            data=ET.tostring(acl_xml, encoding='unicode'),
            headers=headers,
            auth=config['auth'],
            verify=False)
        log_request_response(acl_request, 'modify ACL')


def get_acl(config: dict) -> ET.Element:
    headers = {'Content-Type': 'application/xml'}

    acl_request = requests.get(
        url=config['acl_url'],
        headers=headers,
        auth=config['auth'],
        verify=False)
    log_request_response(acl_request, 'get ACL')

    if acl_request.status_code == 200:
        return ET.fromstring(acl_request.text)
